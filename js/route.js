var route = angular.module("RouteApp",["ngRoute"]);

route.config(function ($routeProvider, $locationProvider) {
            $routeProvider
                    .when('/home', {
                        templateUrl: 'partials/home.html',
                        controller: 'HomeController',
                        controllerAs: 'homeCtrl'
                    })
                    .when('/option', {
                        templateUrl: 'partials/options.html',
                        controller: 'OptionController',
                        controllerAs: 'optionCtrl'
                    })
                    .otherwise({
                        redirectTo: '/home'
                    });

            
            $locationProvider.html5Mode({
                emable:true,
                requireBase:false
            });
        });