var app = angular.module("SuitePadApp",["RouteApp","ngMaterial"]);

/**
 * Constant responsible to define all general keys
 * 
 */
app.constant("KEYS_GENERAL",{
    "URL_ROOT":"http://localhost/1.1"
//    "URL_ROOT":"http://demo.suitepad.systems/1.1"
});

/**
 * Constant reponsible to define all url services from twitter
 * 
 * 
 */
app.constant("KEYS_TWITTER_SERVICES",{
    "TWEETS":"/search/tweets.json"
});

/**
 * Constant reponsible to define all url services from twitter
 * 
 * 
 */
app.constant("KEYS_STORAGE",{
    "CONFIG":"STORAGE_CONFIGURATION"
});

app.filter("MyDateConverter",function(){
    return function (input, dateString ) {
        return (new Date(input)).getTime();    
  };
    
});

/**
 * 
 * This class will be responsible for all Storage Business 
 * 
 * I'm using a 'fluent' declarations of method that will make desnecessary more comments
 * 
 * @param {type} param1
 * @param {type} param2
 */
app.service("Storage",["KEYS_STORAGE",function(KEYS_STORAGE){
    
    var set = function(key,value){
        if(!angular.isObject(value)){
            localStorage.setItem(key,value);
        }else{
            localStorage.setItem(key,JSON.stringify(value));
        }
    };
    
    var get = function(key){
        var result = null;
        try{
            result = JSON.parse(localStorage.getItem(key));
        }catch(e){
            result = localStorage.getItem(key);
        }
        return result;
    };
   
    this.getConfigIfExistsOrReturnNull =  function(){
        var result = get(KEYS_STORAGE.CONFIG);
        if(result!==null){
            return result;
        }
        return  null;
    };
    
    this.updateConfig = function(config){
        set(KEYS_STORAGE.CONFIG,config);
    };
   
        
}]);
/**
 * 
 * 
 * 
 * @param {type} param1
 * @param {type} param2
 */
app.service("SuitePadTwitterAPI",["KEYS_GENERAL","KEYS_TWITTER_SERVICES","$http","Storage",function(K_GENERAL,K_TWITTER,$http,Storage){
   
   var self = this;
   var url="";   
   
    var createParamsSearch = function(){
       var result =  Storage.getConfigIfExistsOrReturnNull();
       url = K_GENERAL.URL_ROOT + K_TWITTER.TWEETS;
       
       if(result!=null){
           url+="?";
           url+="q="+result.text;
           if(result.language!=""){
                url+="&lang="+result.language.toLowerCase();
           }if(result.locationLat!="" && result.locationLong!=""){
                url+="&geo="+result.locationLat+","+result.locationLong+",10mi";           
           }
       }else{
           Storage.updateConfig({"text":"@noradio",language:"",locationLat:"",locationLong:""});
           createParamsSearch();
       }
       
    };
    
   self.getTimeline = function(){
       createParamsSearch();
       var result = null;   
       
       $.ajax({
           async:false,
           method:"GET",
           url:url,
           crossDomain:true,
           ContentType:"application/json"
       }).done(function(response){
          result = response;
       });     
       return result;       
   };  
        
}]);

//app.config(['$httpProvider', function($httpProvider) {
//        $httpProvider.defaults.useXDomain = true;
//        delete $httpProvider.defaults.headers.common['X-Requested-With'];
//    }
//]);

app.controller("HomeController",["$scope","SuitePadTwitterAPI",function($scope,spTwitter){
    
   this.twitter = null; 
   
   this.init = function(){
       this.twitter =  spTwitter.getTimeline().statuses;
       
       
   };
   
   this.init();
    
    
}]);

app.controller("OptionController",["$scope","Storage",function($scope,storage){
    
    $scope.configuration = 
                            {
                                text:"",
                                locationLat:"",
                                locationLong:"",
                                language:""
                            };
    
    
    $scope.$watch('configuration', function (newValue, oldValue, scope) {
        storage.updateConfig(scope.configuration);
    }, true);


   /**
    * Initial Method responsible for all configurations
    * 
    * @returns {undefined}
    */
    this.init = function(){
        var config = storage.getConfigIfExistsOrReturnNull();
        if(config!=null)
            $scope.configuration = config;
    };
   
    this.init();
    
    
}]);